## 聚合类型

### 介绍

- 用于解决多个int值的统一集合,例如Linux的777状态
- 使用与一个状态去判断多个值的情况
- 内含16进制转换,解决多集合int超长的问题

### 安装

1. PHP版本需求 >= 7.0

2. composer安装

   ```shell
   composer require bit_bitwise/bitwise_operations
   ```
### 使用
1. 使用方式

   ```php
   use \Bitwise\BitwiseOperations;
   //实例化对象
   $AggregateType = new BitwiseOperations(30);//可以构造的时候可以传入已聚合的数据
   $AggregateType->aggregate([1,2,3,4]);//聚合数据
   echo $AggregateType->getData().PHP_EOL; //获取聚合后的参数 30
   echo $AggregateType->getDateTo16().PHP_EOL;//获取16进制的数据 1e
   
   echo implode(',',$AggregateType->separate()).PHP_EOL;///获取解析的数据 1,2,3,4
   $AggregateType->carry([7,8,9]);//追加数据
   $AggregateType->reduce([1,3]); //去除数据
   echo $AggregateType->getData().PHP_EOL;//聚合的数据 916
   echo implode(',',$AggregateType->separate()).PHP_EOL;//获取解析的数据 2,4,7,8,9
   ```
   
2. 更多查看测试用例

   ```php
   use TopologicalMapping\AggregateType\test\AggregateTypeTest; 
   //详细使用用例都在用例里
   new AggregateTypeAliasTest();
   ```

## 更新版本

1. 版本:1.0.0
   1. 发布核心功能和测试用例