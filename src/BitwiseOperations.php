<?php
declare(strict_types=1);

namespace Bitwise;

/**
 * @BitwiseOperations
 * @time 2023/11/25 10:02
 * @remark: 对多个int数据,的集合封装
 */
class BitwiseOperations{

    /**
     * 聚合
     * @var int
     */
    private $Aggregate;

    public function __construct(int $Aggregate = 0,$config = [])
    {
        $this->Aggregate = $Aggregate;
    }

    /**
     * @name:聚合
     * @interface Aggregate
     * @param $value
     * @return $this
     * @time   2023/11/22 23:53
     * @remark: 多个int聚合城一个集合数字
     * @rules:  规则~
     */
    public function aggregate($value):self
    {
        $value = is_array($value)?$value:[$value];
        self::carry($value);
        return $this;
    }

    /***
     * @name:解密
     * @interface separate
     * @return int[]
     * @time   2023/11/23 0:02
     * @remark: 描述~
     * @rules:  规则~
     */
    public function separate():array
    {
        //二进制解压
        $binaryArray = self::transition();
        return  array_keys(array_intersect($binaryArray,[1]));
    }

    /**
     * @name:步进
     * @interface carry
     * @param $value
     * @return $this
     * @time   2023/11/23 0:02
     * @remark: 描述~
     * @rules:  规则~
     */
    public function carry($value):self
    {
        $value  = is_array($value)?$value:[$value];
        $this->Aggregate = $this->Aggregate | array_reduce($value,function ($carry, $item){
            return $carry | (1 << $item);
        },0);
        return $this;
    }

    /***
     * @name:减少
     * @interface reduce
     * @param $value
     * @return $this
     * @time   2023/11/23 0:04
     * @remark: 描述~
     * @rules:  规则~
     */
    public function reduce($value):self
    {
        $value  = is_array($value)?$value:[$value];
        $this->Aggregate = $this->Aggregate  & ~(array_reduce($value,function ($carry, $item){
                return $carry | (1 << $item);
            },0));
        return $this;
    }

    /***
     * @name:是否存在
     * @interface has
     * @param  int|null $value
     * @return bool
     * @time   2023/11/23 0:04
     * @remark: 描述~
     * @rules:  规则~
     */
    public function has($value = null):bool
    {
        if (empty($this->Aggregate)) return false;
        $binary = self::transition()[$value]??false;
        return $binary == 1;
    }

    /**
     * @name:长度
     * @interface count
     * @return int
     * @time   2023/11/23 19:33
     * @remark: 描述~
     * @rules:  规则~
     */
    public function count():int
    {
        return self::transition()?count(self::transition()):0;
    }

    /**
     * @name:转换成数组
     * @interface transition
     * @return array
     * @time   2023/11/23 0:04
     * @remark: 描述~
     * @rules:  规则~
     */
    private function transition():array
    {
        $binaryString = decbin($this->Aggregate);
        return str_split(strrev($binaryString));
    }


    /**
     * @name:写入数据
     * @interface setData
     * @param $value
     * @return $this
     * @time   2023/11/23 0:07
     * @remark: 描述~
     * @rules:  规则~
     */
    public function setData(int $value):self
    {
        $this->Aggregate = $value;
        return $this;
    }

    /**
     * @name:获取数据
     * @interface getData
     * @return int
     * @time   2023/11/23 0:07
     * @remark: 描述~
     * @rules:  规则~
     */
    public function getData():int
    {
        return $this->Aggregate;
    }

    /**
     * @name:写入16进制的数据
     * @interface setDataType16
     * @param $string
     * @return $this
     * @time   2023/11/24 0:12
     * @remark: 描述~
     * @rules:  规则~
     */
    public function setDataTo16($string):self
    {
        $this->setData(hexdec($string));
        return $this;
    }

    /**
     * @name:以16进制输出
     * @interface getDateTo16
     * @return string
     * @time   2023/11/24 0:11
     * @remark: 描述~
     * @rules:  规则~
     */
    public function getDateTo16():string
    {
        return dechex($this->getData());
    }


}
