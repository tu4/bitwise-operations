<?php
namespace Bitwise\tests;

use Bitwise\BitwiseOperations;

/**
 * 聚合的测试用例
 */
class BitwiseOperationsTests{

    /**
     * @name:聚合多个数组
     * @interface Aggregate
     * @return mixed
     * @time   2023/11/25 10:04
     * @remark: 描述~
     * @rules:  规则~
     */
    public function aggregateTest(){
        return (new BitwiseOperations())->aggregate([
            1,2,3,5,6,7
        ]);
    }

    /**
     * @name:解压多个聚合数据
     * @interface SeparateTest
     * @return int[]
     * @time   2023/11/25 10:15
     * @remark: 描述~
     * @rules:  规则~
     */
    public function separateTest(){
        $mode = new BitwiseOperations();
        $res  = $mode->aggregate([
            1,2,3,5,6,7
        ]);
        return $res->separate();
    }

    /**
     * @name:步进数据集合
     * @interface carryTest
     * @return int[]
     * @time   2023/11/25 10:26
     * @remark: 描述~
     * @rules:  规则~
     */
    public function carryTest(){
        $mode = new BitwiseOperations();
        $res  = $mode->aggregate([
            1,2,3,5,6,7
        ]);
        return $res->carry([11,13])->separate();
    }

    /**
     * @name:删除指定的参数
     * @interface reduceTest
     * @return int[]
     * @time   2023/11/25 10:31
     * @remark: 描述~
     * @rules:  规则~
     */
    public function reduceTest(){
        $mode = new BitwiseOperations();
        $res  = $mode->aggregate([
            1,2,3,5,6,7
        ]);
        return $res->reduce(1)->separate();
    }

    /**
     * @name:判断参数是否存在
     * @interface hasTest
     * @return bool
     * @time   2023/11/25 10:32
     * @remark: 描述~
     * @rules:  规则~
     */
    public function hasTest(){
        $mode = new BitwiseOperations();
        $res  = $mode->aggregate([
            1,2,3,5,6,7
        ]);
        return $res->has(1);
    }

    /**
     * @name:获取集合的总长度
     * @interface countTest
     * @return int
     * @time   2023/11/25 10:33
     * @remark: 描述~
     * @rules:  规则~
     */
    public function countTest(){
        $mode = new BitwiseOperations();
        $res  = $mode->aggregate([
            1,2,3,5,6,7
        ]);
        return $res->count();
    }
}